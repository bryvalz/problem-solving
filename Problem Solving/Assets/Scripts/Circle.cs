﻿using UnityEngine;

public class Circle : MonoBehaviour
{
    private Rigidbody2D rigidBody2D;

    [Header("Random Move")]
    public bool randomMovement;
    public float randomMoveSpeed;

    [Header("Keyboard Move")]
    public bool keyboardMovement;
    public float keyMoveSpeed;

    [Header("Mouse Move")]
    public bool mouseMovement;
    public float mouseMoveSpeed;

    private void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        //random move
        if (randomMovement)
        {
            Invoke("MoveCircle", 1);
        }
    }

    private void Update()
    {
        //input dari keyboard
        if (keyboardMovement)
        {
            float xMovement = Input.GetAxis("Horizontal");
            float yMovement = Input.GetAxis("Vertical");

            transform.position += new Vector3(xMovement, yMovement) * Time.deltaTime * keyMoveSpeed;
        }

        //follow posisi dari cursor
        if (mouseMovement)
        {
            Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = Vector2.Lerp(transform.position, cursorPos, mouseMoveSpeed);
        }

        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            ResetMovement();
        }

        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            ResetMovement();

            randomMovement = true;
            Invoke("MoveCircle", 1);
        }

        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            ResetMovement();

            randomMovement = true;
            Invoke("MoveCircle", 1);
        }

        if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            ResetMovement();

            keyboardMovement = true;
        }

        if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            ResetMovement();

            mouseMovement = true;
        }
    }

    void MoveCircle()
    {
        float x = Random.value < 0.5f ? -1f : 1f;

        Vector2 direction = new Vector2(x, Random.Range(-1f, 1f));
        rigidBody2D.AddForce(direction * randomMoveSpeed);        
    }

    void ResetMovement()
    {
        rigidBody2D.velocity = Vector3.zero;

        randomMovement = false;
        keyboardMovement = false;
        mouseMovement = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Box"))
        {
            Destroy(collision.gameObject);
            GameManager.Instance.score += 1;
            GameManager.Instance.objectSpawnCount -= 1;
        }
    }
}
