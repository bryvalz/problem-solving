﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    private int boxCount;
    private float spawnTimeDelay = 3f;
    private bool timeTrial;
    private bool spawnBoxes;

    //private Circle circleScript;

    [Header("Game Properties")]
    public int score;
    public Text scoreValue;
    public float time;
    public Text timeValue;
    public GameObject wall;

    [Header("Game Over Properties")]
    public bool gameOver;
    public GameObject gameOverPanel;
    public int hiScore;
    public Text scoreValueOver;
    public Text hiScoreValueOver;

    [Header("Circle Properties")]
    public GameObject circleObject;
    public Vector3 circleStartPos;

    [Header("Square Properties")]
    public GameObject squareObject;
    public int objectSpawnCount;
    public int maxObjectSpawn;
    public int minObjectSpawn;
    public float maxXPos;
    public float maxYPos;

    private void Start()
    {
        timeTrial = true;
        spawnBoxes = true;

        RestartGame();              
    }

    private void Update()
    {
        //update score        
        scoreValue.text = "Score : " + score.ToString();

        //time
        if (!gameOver && timeTrial)
        {
            time -= Time.deltaTime;
            timeValue.text = time.ToString("F1");
        }        

        //spawn box
        if (objectSpawnCount < boxCount && spawnBoxes)
        {
            spawnTimeDelay -= Time.deltaTime;

            if (spawnTimeDelay <= 0)
            {
                Vector3 squareSpawnPos = new Vector3(Random.Range(-maxXPos, maxXPos), Random.Range(-maxYPos, maxYPos), 0);
                if ((squareSpawnPos - circleObject.transform.position).magnitude > 5)
                {
                    Instantiate(squareObject, squareSpawnPos, Quaternion.identity);
                    spawnTimeDelay = 3f;
                    objectSpawnCount++;
                }                
            }
        }

        //game Over
        if (time <= 0)
        {
            gameOver = true;

            //show game over panel
            gameOverPanel.SetActive(true);

            //destroy lingkaran ddan box yang tersisa
            DestroyCircle();
            DestroyBox();

            //set hi score
            if (hiScore == 0)
            {
                hiScore = score;
            }
            else
            {
                if (hiScore >= score)
                {
                    hiScoreValueOver.text = hiScore.ToString();
                    scoreValueOver.text = score.ToString();
                }
                else
                {
                    hiScore = score;
                    hiScoreValueOver.text = score.ToString();
                    scoreValueOver.text = score.ToString();
                }
            }
        }

        //Input untuk memilih build
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            HideTimerAndScore(false, false);

            ResetCircle();

            DestroyBox();
            spawnBoxes = false;

            wall.SetActive(false);
        }

        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            HideTimerAndScore(false, false);

            ResetCircle();

            DestroyBox();
            spawnBoxes = false;

            wall.SetActive(false);
        }

        if (Input.GetKeyUp(KeyCode.Alpha3) || Input.GetKeyUp(KeyCode.Alpha4) || Input.GetKeyUp(KeyCode.Alpha5))
        {
            HideTimerAndScore(false, false);

            ResetCircle();

            DestroyBox();
            spawnBoxes = false;

            wall.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            HideTimerAndScore(false, false);

            ResetCircle();

            DestroyBox();
            spawnBoxes = false;
            SpawnBoxes();

            wall.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            HideTimerAndScore(false, true);

            ResetCircle();

            DestroyBox();
            spawnBoxes = false;
            SpawnBoxes();

            wall.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            HideTimerAndScore(false, true);

            ResetCircle();

            DestroyBox();
            spawnBoxes = true;
            SpawnBoxes();

            wall.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            HideTimerAndScore(true, true);

            ResetCircle();

            DestroyBox();
            spawnBoxes = true;
            SpawnBoxes();

            timeTrial = true;

            wall.SetActive(true);
        }

        //tombol escape to quit
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void SpawnCircle()
    {
        //Spawn circle
        Instantiate(circleObject, circleStartPos, Quaternion.identity);
    }

    void ResetCircle()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = circleStartPos;
    }

    void SpawnBoxes()
    {
        //set jumlah box yang mau di spoawn
        objectSpawnCount = Random.Range(minObjectSpawn, maxObjectSpawn);
        boxCount = objectSpawnCount;

        //spawn
        for (int i = 0; i < objectSpawnCount; i++)
        {
            Vector3 squareSpawnPos = new Vector3(Random.Range(-maxXPos, maxXPos), Random.Range(-maxYPos, maxYPos), 0);
            Instantiate(squareObject, squareSpawnPos, Quaternion.identity);
        }
    }

    public void RestartGame()
    {
        gameOver = false;
        gameOverPanel.SetActive(false);

        score = 0;
        time = 20f;

        SpawnCircle();
        SpawnBoxes();
    }

    void DestroyCircle()
    {
        //destroy lingkaran
        Destroy(GameObject.FindGameObjectWithTag("Player"));
    }

    public void DestroyBox()
    {    
        //destroy box yang tersisa
        GameObject[] boxes = GameObject.FindGameObjectsWithTag("Box");
        for (int i = 0; i < boxes.Length; i++)
        {
            Destroy(boxes[i]);
        }
    }

    void HideTimerAndScore(bool valueTimer, bool valueScore)
    {
        timeTrial = false;
        time = 20f;
        timeValue.gameObject.SetActive(valueTimer);

        score = 0;
        scoreValue.gameObject.SetActive(valueScore);
    }
}
