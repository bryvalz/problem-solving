﻿using UnityEngine;

public class GameOver : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            GameManager.Instance.RestartGame();
        }
    }
}
